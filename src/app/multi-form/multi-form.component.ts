import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multi-form',
  templateUrl: './multi-form.component.html',
  styleUrls: ['./multi-form.component.css']
})
export class MultiFormComponent implements OnInit {
  private data: any[] = new Array();
  private lastData: any[] = new Array();
   steps: number;
  private stepLength: number;
  private currentStep: number;
  private progress: number;
  private completed: boolean;

  constructor() {
    this.currentStep = 1;
    this.stepLength = 100/this.steps;
    this.progress = this.stepLength * this.currentStep;
    this.completed = false;
  }

  ngOnInit() {
        this.steps = 3;
  }

  nextStep(form) {
    if(form) {
      this.setData(form);
      this.renderLastData();
      this.currentStep++;
      this.getProgress();
      this.isCompleted();
    }
  }

  prevStep(isBack: boolean): void {
    this.currentStep--;
    this.getProgress();
  }

  setData(data: any[]): void {
    this.data.splice(this.currentStep - 1, 1, data);
  }

  renderLastData(): void {
    this.lastData = this.data[this.currentStep - 1];
  }

  getProgress() {
    this.progress = this.stepLength * this.currentStep;
    return 5;
  }

  isCompleted(): void {
    if(this.currentStep === this.steps) {
      this.completed = true;
    }
  }
}
