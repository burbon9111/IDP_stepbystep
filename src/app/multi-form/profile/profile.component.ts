import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Profile } from './profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Output() onNextStep = new EventEmitter<boolean>();
  @Output() onPrevStep = new EventEmitter<boolean>();
  @Input() stepNumber: number;

  private profile: Profile = new Profile();

  constructor() {
  }

  ngOnInit() {
  }

  nextStep(form: any): void{
    if(form.valid) {
      this.onNextStep.emit(form.value);
    }
  }

  prevStep(): void {
    this.onPrevStep.emit(true);
  }
}
