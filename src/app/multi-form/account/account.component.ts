import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Account } from './account';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  @Output() onNextStep = new EventEmitter<boolean>();
  @Input() stepNumber: number;

  private account: Account = new Account();

  constructor() {
  }

  ngOnInit() {
  }

  nextStep(form: any): void{
    if(form.valid) {
      this.onNextStep.emit(form.value);
    }
  }
}
