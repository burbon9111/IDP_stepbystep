import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MultiFormComponent } from './multi-form/multi-form.component';
import { AccountComponent } from './multi-form/account/account.component';
import { ProfileComponent } from './multi-form/profile/profile.component';
import { ToArrayPipe } from './multi-form/to-array.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MultiFormComponent,
    AccountComponent,
    ProfileComponent,
    ToArrayPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
